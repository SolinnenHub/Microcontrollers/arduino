int analogPin = A0;

float alpha = 0.03;
float smoothed = 0;
int counter = 0;
int global_counter = 0;
bool filled = false;
long int t;
 
const int buffer_size = 820;
int buffer[buffer_size];

const int pattern_buffer_size = 4;
int pattern_buffer[pattern_buffer_size];
int pattern_pos = 0;

void setup() {
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(analogPin, INPUT);
  Serial.begin(9600);

  for (int i = 0; i < buffer_size; i++) {
    buffer[i] = 0;
  }

  for (int i = 0; i < pattern_buffer_size; i++) {
    pattern_buffer[i] = 0;
  }

  t = millis();
}

void loop() {
  int raw = analogRead(analogPin);

  buffer[global_counter] = raw;
  global_counter = (global_counter + 1) % buffer_size;
  if (global_counter == 0) {
    filled = true;
  }

  unsigned long long s_buffer = 0;
  if (filled) {
    for (int i = 0; i < buffer_size; i++) {
      s_buffer += buffer[i];
    }
  }
  int avg_buffer = s_buffer / buffer_size;
  
  int norm = raw - avg_buffer;
  float ab = abs(norm);
  if (ab > smoothed) {
    smoothed = ab;
  } else {
    smoothed = alpha*ab + (1 - alpha)*smoothed;
  }

  if (smoothed > 150) {
    digitalWrite(3, HIGH);
    if (smoothed > 300) {
      digitalWrite(4, HIGH);
      if (smoothed > 460) {
        digitalWrite(5, HIGH);
        if (smoothed > 512) {
          digitalWrite(6, HIGH);
        } else {
          digitalWrite(6, LOW);
        }
      } else {
        digitalWrite(5, LOW);
        digitalWrite(6, LOW);
      }
    } else {
      digitalWrite(4, LOW);
      digitalWrite(5, LOW);
      digitalWrite(6, LOW);
    }
  } else {
     digitalWrite(3, LOW);
     digitalWrite(4, LOW);
     digitalWrite(5, LOW);
     digitalWrite(6, LOW);
  }

  if (millis() - t > 3000) {
    digitalWrite(7, LOW);
    pattern_pos = 0;
    for (int i = 0; i < pattern_buffer_size; i++) {
      pattern_buffer[i] = 0;
    }
  }


  //////////////

  if (smoothed > 150) {
    long int now = millis();
    long int delta = now - t;
    t = now;
    if (delta > 80) {
      if (pattern_pos == 0) {
        pattern_pos = (pattern_pos + 1) % (pattern_buffer_size + 1);
      } else {
         pattern_buffer[pattern_pos - 1] = delta;
         pattern_pos = (pattern_pos + 1) % (pattern_buffer_size + 1);
      }

      for (int i = 0; i < pattern_buffer_size; i++) {
        Serial.print(pattern_buffer[i]);
        Serial.print(" ");
      }
      Serial.println();
      
      if (pattern_buffer[pattern_buffer_size - 1] > 0) {
        int s = pattern_buffer[0] + pattern_buffer[1] + pattern_buffer[2];
        int div = abs(pattern_buffer[0] - s / 3) + abs(pattern_buffer[1] - s / 3) + abs(pattern_buffer[2] - s / 3);
        Serial.println(div);
        if (pattern_buffer[3] > 2*s/3 && div < 100) {
          digitalWrite(7, HIGH);
        }
      }
    }
  }
}