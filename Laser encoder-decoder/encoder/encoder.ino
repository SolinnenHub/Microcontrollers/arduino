#include <Keypad.h>

const byte ROWS = 4;
const byte COLS = 3;
const byte led = 10;

char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};
byte rowPins[ROWS] = {9, 8, 7, 6};
byte colPins[COLS] = {5, 4, 3};

byte c0[] = {0,1,1,0,1};

int T = 24;

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

void setup(){
  Serial.begin(9600);
  pinMode(led, OUTPUT);
}

void output(char c) {
    for (int i = 7; i >= 0; --i) {
      if (c & (1 << i)) {
        digitalWrite(led, HIGH);
        delay(T / 2);
        digitalWrite(led, LOW);
        delay(T / 2);

      } else {
        digitalWrite(led, HIGH);
        delay(T);
        digitalWrite(led, LOW);
      }
    }
    digitalWrite(led, LOW);
}

  
void loop(){
  char key = keypad.getKey();
  
  if (key) {
    output(key);
  }
}




