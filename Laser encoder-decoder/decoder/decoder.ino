#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3); // RX, TX

#define PIN_PHOTO_SENSOR A0
int led = 7;
float T = 24;
float error = 0.25;

int raw = 0;
long int t0;
long int t1;
int lock = 0;
char c = 0;
int bits_received = 0;

void setup() {
  Serial.begin(115200);
  mySerial.begin(115200);
  pinMode(led, OUTPUT);
  t0 = millis();
}

void loop() {
  raw = analogRead(PIN_PHOTO_SENSOR);
  digitalWrite(led, lock);

  if (raw < 50) {
    if (lock == 0) {
      lock = 1;

      long int t = millis();

      if (t - t1 > T) {
        c = 0;
        bits_received = 0;
      }

      t0 = t;
    }

  } else {
    if (lock == 1) {
      lock = 0;
      t1 = millis();
      float k = (t1 - t0) / T;
      if (k < error) {
        return;
      }
      int q = int(k);
      float r = k - q;

      for (int i = 0; i < q; i++) {
        c = c << 1;
      }
      bits_received += q;

      if (error < r) {
        c = c << 1;
        c |=  1;
        bits_received += 1;
      }

      if (bits_received >= 8) {
        mySerial.write(c);
        c = 0;
        bits_received = 0;
      }
    }
  }
}
